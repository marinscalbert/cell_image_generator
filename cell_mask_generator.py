import numpy as np

from sklearn.mixture import GaussianMixture
from skimage.draw import polygon
from skimage.util import pad

from elliptical_fourier_shape_descriptors import compute_efsd


class CellMaskGenerator():
    """
    This class implements the cell segmentation mask generator
    """

    def __init__(self, N=30, K=20):
        """

        Args:
            N (int, optional): Number of harmonics to consider for the
                computation of elliptical fourier descriptors
            K (int, optional): Number of components for density
                estimation with the gaussian mixture model

        """
        self._pdf_descriptors = None
        self.harmonic_count = N
        self.component_count = K

    @property
    def pdf_descriptors(self):
        return self._pdf_descriptors

    def fit(self, cell_masks, nucleus_masks):
        """Approximate the joint probability density function of cell and
        nucleus shapes descriptors with a gaussian mixture model.

        Args:
            cell_masks (list): List of cell binary segmentation masks
            nucleus_masks (list): List of nucleus binary segmentation masks

        """
        X = []
        for cell_mask, nucleus_mask in zip(cell_masks, nucleus_masks):
            cell_efsd = compute_efsd(cell_mask, self.harmonic_count)
            nucleus_efsd = compute_efsd(nucleus_mask, self.harmonic_count)
            X.append(np.concatenate([cell_efsd, nucleus_efsd]))

        self._pdf_descriptors = GaussianMixture(
            n_components=self.component_count,
            covariance_type='full').fit(np.array(X))

    def sample(self):
        """Sample EFDS.

        Returns:
            tuple: (cell contour descriptors, nucleus contour descriptors)

        """
        morphology_descriptors = self.pdf_descriptors.sample()[0][0, :]

        descriptors_count = len(morphology_descriptors)//2
        cell_descriptors = morphology_descriptors[:descriptors_count]
        nucleus_descriptors = morphology_descriptors[descriptors_count:]
        return cell_descriptors, nucleus_descriptors

    def generate_cell_mask(self, cell_contour, nucleus_contour, padding=10):
        """Generate a cell mask from a cell contour, nucleus contour
            and a padding around the cell

        Args:
            cell_contour (ndarray): Cell contour coordinates of shape (K,2).
            nucleus_contour (ndarray): Nucleus contour coordinates of shape
                                       (K,2).
            padding (int): Padding size to considered around the cell.

        Returns:
            ndarray: Cell mask
        """
        min_row = np.minimum(
            np.min(cell_contour[:, 0]), np.min(nucleus_contour[:, 0]))
        min_col = np.minimum(
            np.min(cell_contour[:, 1]), np.min(nucleus_contour[:, 1]))

        cell_contour[:, 0] -= min_row
        nucleus_contour[:, 0] -= min_row
        cell_contour[:, 1] -= min_col
        nucleus_contour[:, 1] -= min_col

        height = np.ceil(
            np.maximum(
                np.max(cell_contour[:, 0]), np.max(nucleus_contour[:, 0]))
        ).astype(int)
        width = np.ceil(
            np.maximum(np.max(cell_contour[:, 1]),
                       np.max(nucleus_contour[:, 1]))
        ).astype(int)
        generated_mask = np.zeros((height,  width))
        rr, cc = polygon(cell_contour[:, 1], cell_contour[:, 0])
        generated_mask[cc, rr] = 128
        rr, cc = polygon(nucleus_contour[:, 1], nucleus_contour[:, 0])
        generated_mask[cc, rr] = 255

        generated_mask = pad(generated_mask,
                             pad_width=((padding, padding),
                                        (padding, padding)),
                             mode="constant")

        return generated_mask
