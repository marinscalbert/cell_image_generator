import numpy as np
from skimage.measure import points_in_poly
from elliptical_fourier_shape_descriptors import (get_cell_efsd,
                                                  reconstruct_contour)


def generate_cell(
        xs,
        ms,
        cell_mask_generator,
        doodle_style_transfer,
        max_iter=1000,
        tol=1e-1,
        padding=10,
        K=100):
    """Generate a synthetic cell image.

    Args:
        xs (ndarray): texture cell image.
        ms (ndarray): cell segmentation.mask.
        cell_mask_generator (CellMaskGenerator): Trained CellMaskGenerator
                instance
        max_iter (int): maximum number of elliptical fourrier
            shape descriptors (EFD) sampling.
        tol (float): maximum euclidean distance between the input mask
            EFD and the sampled EFD.
        padding (int, optional): Description
        K (int): Number of discretization points for curve reconstruction

    Returns:
        xt (ndarray): synthetic cell image.

    """

    ms_efsd = get_cell_efsd(
        ms,
        cell_mask_generator.pdf_descriptors.means_.shape[-1]//8)

    for _ in range(max_iter):
        mt_efsd = cell_mask_generator.pdf_descriptors.sample()[0][0, :]
        mid = int(len(mt_efsd)//2)
        cell_efsd = mt_efsd[:mid]
        nucleus_efsd = mt_efsd[mid:]
        cell_contour = reconstruct_contour(cell_efsd, K)
        nucleus_contour = reconstruct_contour(nucleus_efsd, K)
        nucleus_is_in_cell = points_in_poly(
                nucleus_contour,
                cell_contour).all()
        if nucleus_is_in_cell:
            diff = mt_efsd-ms_efsd
            err = np.sqrt(diff.dot(diff))
            if err < tol*len(mt_efsd):
                break

    padding += 15
    mt = cell_mask_generator.generate_cell_mask(
        cell_contour,
        nucleus_contour,
        padding=padding)
    xt = doodle_style_transfer.train(xs, ms, mt)
    return xt[15:-15, 15:-15, :], mt[15:-15, 15:-15]
