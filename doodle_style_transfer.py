import time
import warnings
import numpy as np
import tensorflow as tf
from scipy.optimize import fmin_l_bfgs_b
from skimage.transform import resize
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Input, AveragePooling2D
from tensorflow.keras.models import Model
from tensorflow.keras.applications import vgg19


class Evaluator(object):

    def __init__(self, neural_doodle):
        self.neural_doodle = neural_doodle
        self.loss_value = None
        self.grads_values = None

    def loss(self, x):
        assert self.loss_value is None
        loss_value, grad_values = self.neural_doodle.eval_loss_and_grads(x)
        self.loss_value = loss_value
        self.grad_values = grad_values
        return self.loss_value

    def grads(self, x):
        assert self.loss_value is not None
        grad_values = np.copy(self.grad_values)
        self.loss_value = None
        self.grad_values = None
        return grad_values


class DoodleStyleTransfer(object):

    """
    This class implements the doodle style transfer model described in
    https://arxiv.org/pdf/1603.01768.pdf
    """

    def __init__(self):
        super(DoodleStyleTransfer, self).__init__()

    def preprocess_image(self, image, target_size):
        # Substract to an image the mean calculated on the training
        # set used for the VGG training
        img = resize(image, output_shape=target_size, order=0,
                     preserve_range=True)
        img = np.expand_dims(img, axis=0)
        img = vgg19.preprocess_input(img)
        return img

    def deprocess_image(self, x):
        # Add to an image the mean calculated on the training set used
        # for the VGG training
        if K.image_data_format() == 'channels_first':
            x = x.reshape((self.image_depth, self.image_height,
                           self.image_width))
            x = x.transpose((1, 2, 0))
        else:
            x = x.reshape((self.image_height, self.image_width,
                           self.image_depth))
        # Remove zero-center by mean pixel
        x[:, :, 0] += 103.939
        x[:, :, 1] += 116.779
        x[:, :, 2] += 123.68
        # 'BGR'->'RGB'
        x = x[:, :, ::-1]
        x = np.clip(x, 0, 255).astype('uint8')
        return x

    def build_mask_model(self, mask_input):
        x = mask_input
        for layer in self.image_model.layers[1:]:
            name = 'mask_%s' % layer.name
            if 'conv' in layer.name:
                x = AveragePooling2D((3, 3), padding='same', strides=(
                    1, 1), name=name)(x)
            elif 'pool' in layer.name:
                x = AveragePooling2D((2, 2), name=name)(x)
        mask_model = Model(mask_input, x)
        return mask_model

    def extract_patches(self, x, patch_shape, strides):
        patch_height, patch_width, patch_depth = patch_shape
        B, H, W, C = K.int_shape(x)
        assert patch_depth == C
        patches = tf.extract_image_patches(
            images=x,
            ksizes=[1, patch_height, patch_width, 1],
            strides=[1, strides[0], strides[1], 1],
            rates=[1, 1, 1, 1],
            padding="VALID")
        patches = tf.reshape(patches, (-1, patch_height, patch_width,
                                       patch_depth))
        patches_norm = K.sqrt(K.sum(K.square(patches), axis=(1, 2, 3)))
        patches_norm = K.reshape(patches_norm,
                                 (K.int_shape(patches_norm)[0], 1, 1, 1))
        return patches, patches_norm

    def find_nearest_neighbor_patches(self, query_patches, patches):
        query_patches = K.reshape(query_patches,
                                  (K.int_shape(query_patches)[0], -1))
        patches = K.reshape(patches, (K.int_shape(patches)[0], -1))
        matrix_similarities = K.dot(query_patches, K.transpose(patches))
        indexes_style_nearest_neighbors = K.cast(
            K.argmax(matrix_similarities, axis=-1), tf.int32)
        return indexes_style_nearest_neighbors

    def get_mrf_loss(self, style_features, norm_style_features,
                     target_features, norm_target_features, mrf_style_weights):
        mrf_style_weights = (np.array(mrf_style_weights) /
                             np.sum(mrf_style_weights))
        mrf_loss = K.variable(0.0)
        for (style_patches, norm_style_patches, target_patches,
             norm_target_patches, mrf_style_weight) in zip(
                style_features,
                norm_style_features,
                target_features,
                norm_target_features,
                mrf_style_weights):
            mrf_loss = mrf_loss \
                       + (mrf_style_weight * self.get_layer_mrf_loss(
                            style_patches,
                            norm_style_patches,
                            target_patches,
                            norm_target_patches))
        return mrf_loss

    def get_layer_mrf_loss(self, style_patches, norm_style_patches,
                           target_patches, norm_target_patches):
        normalized_style_patches = style_patches / norm_style_patches
        normalized_target_patches = target_patches / norm_target_patches
        indexes_style_nearest_neighbors = self.find_nearest_neighbor_patches(
            normalized_target_patches, normalized_style_patches)
        layer_mrf_loss = K.sum(
            K.sum(K.square(target_patches -
                           K.gather(style_patches,
                                    indexes_style_nearest_neighbors)),
                  axis=(1, 2, 3)))
        return layer_mrf_loss

    def get_total_variation_loss(self, x):
        assert 4 == K.ndim(x)
        if K.image_data_format() == 'channels_first':
            a = K.square(
                x[:, :, :self.image_height - 1, :self.image_width - 1] -
                x[:, :, 1:, :self.image_width - 1])
            b = K.square(
                x[:, :, :self.image_height - 1, :self.image_width - 1] -
                x[:, :, :self.image_height - 1, 1:])
        else:
            a = K.square(
                x[:, :self.image_height - 1, :self.image_width - 1, :] -
                x[:, 1:, :self.image_width - 1, :])
            b = K.square(
                x[:, :self.image_height - 1, :self.image_width - 1, :] -
                x[:, :self.image_height - 1, 1:, :])
        return K.sum(K.pow(a + b, 1.25))

    def eval_loss_and_grads(self, x):
        if K.image_data_format() == 'channels_first':
            x = x.reshape((1, self.image_depth, self.image_height,
                           self.image_width))
        else:
            x = x.reshape((1, self.image_height, self.image_width,
                           self.image_depth))
        outs = self.f_outputs([x])
        loss_value = outs[0]
        if len(outs[1:]) == 1:
            grad_values = outs[1].flatten().astype('float64')
        else:
            grad_values = np.array(outs[1:]).flatten().astype('float64')
        return loss_value, grad_values

    def train(self,
              xs,
              ms,
              mt,
              mrf_style_layers=['block1_conv1', 'block1_conv2',
                                'block2_conv1', 'block2_conv2'],
              mrf_style_weights=[1, 1, 1, 1],
              total_variation_weight=20,
              gammas=[5000, 5000, 5000, 5000],
              N_max=30,
              patch_shapes=[(3, 3), (3, 3), (3, 3), (3, 3)],
              strides=[(1, 1), (1, 1), (1, 1), (1, 1)]):
        """This function transfers the different textures of xs delimited by
        the segmentation mask ms onto a given segmentation mask mt.

        Args:
            xs (ndarray): Reference image of shape (Hs, Ws, 3)
            ms (ndarray): Segmentation masks of reference image of
                shape (Hs, Ws)
            mt (ndarray): Segmentation mask of the generated image of
                shape (Ht, Wt)
            mrf_style_layers (list): List of VGG19 layer names to
                consider for the MRF loss
            mrf_style_weights (list): List of weights for MRF loss terms
            total_variation_weight (float): Hyperparameter for total
                variation loss importance
            gammas (list): Hyperparameters to control semantic
                information. The downsampled masks are multipled by those
                values before being concatenating to features maps
            N_max (int): Number of maximum iterations for
                LBFGS optimization
            patch_shapes (list): List of neural patches size to extract
                respectively on the outputs of the layers mrf_style_layers
            strides (list): List of stride dimensions for the neural patches
                extraction.

        Returns:
            (ndarray): Generated image xt representing the transfer of
                the different textures of xs delimited by the segmentation
                mask ms onto the given segmentation mask mt

        Raises:
            ValueError: A ValueError is raised any label value of the mask mt
                does not appear in the mask ms.

        """

        # First we need to resize the target mask in order to have a
        # target mask which have the same size as the style mask

        ms_vals = np.unique(ms)
        mt_vals = np.unique(mt)
        if len(np.setdiff1d(mt_vals, ms_vals, True)) > 0:
            raise ValueError("Generated mask values does" +
                             "not match Reference mask values.")

        ms = np.stack([ms == v for v in ms_vals], axis=-1).astype(np.float32)
        mt = np.stack([mt == v for v in ms_vals], axis=-1).astype(np.float32)

        self.image_height, self.image_width, self.image_depth = list(mt.shape[
                                                                     :-1])+[3]

        # Create placeholder for target image, Preprocess style image
        xs_tensor = K.variable(self.preprocess_image(xs, xs.shape))
        ms = K.variable(ms, dtype="float32")
        mt = K.variable(mt, dtype="float32")

        target_image_tensor = K.placeholder(
            shape=[1] + list(K.int_shape(mt)[:-1]) + [3])

        # Cast xs_tensor and target_image
        xs_tensor = tf.cast(xs_tensor, tf.float32)
        target_image_tensor = tf.cast(target_image_tensor, tf.float32)

        ms = K.expand_dims(ms, axis=0)
        mt = K.expand_dims(mt, axis=0)

        # Create VGG model and Mask model
        vgg = vgg19.VGG19(include_top=False, weights="imagenet")
        self.image_model = Model(vgg.input,
                                 [layer.output for layer in vgg.layers
                                  if layer.name in mrf_style_layers])
        features_maps_style = self.image_model(xs_tensor)
        features_maps_target = self.image_model(target_image_tensor)

        mask_input = Input(shape=(None, None, None), name='mask_input')
        self.mask_model = self.build_mask_model(mask_input)
        self.mask_model = Model(
            self.mask_model.input,
            [layer.output for layer in self.mask_model.layers
             if layer.name.replace("mask_", "") in mrf_style_layers])

        # Compute feature maps from VGG, downsampled masks from mask model and
        # concatenate volume for each layer
        downsampled_masks_style = self.mask_model(ms)
        downsampled_masks_target = self.mask_model(mt)

        downsampled_masks_style = [gamma * downsampled_mask for (
            gamma, downsampled_mask) in zip(gammas, downsampled_masks_style)]
        downsampled_masks_target = [gamma * downsampled_mask for (
            gamma, downsampled_mask) in zip(gammas, downsampled_masks_target)]

        style_features = [
            K.expand_dims(K.concatenate(
                [features_map[0, :, :, :], downsampled_mask[0, :, :, :]],
                axis=-1), axis=0)
            for (features_map, downsampled_mask)
            in zip(features_maps_style, downsampled_masks_style)]
        target_features = [
            K.expand_dims(
                K.concatenate(
                    [features_map[0, :, :, :], downsampled_mask[0, :, :, :]],
                    axis=-1),
                axis=0)
            for (features_map, downsampled_mask)
            in zip(features_maps_target, downsampled_masks_target)]

        # Extract all patches from concatened representations for each layer
        style_patches = []
        norm_style_patches = []
        target_patches = []
        norm_target_patches = []
        for i in range(len(mrf_style_layers)):
            style_p, norm_style_p = self.extract_patches(
                style_features[i],
                patch_shape=(patch_shapes[i][0], patch_shapes[i][1],
                             K.int_shape(style_features[i])[-1]),
                strides=strides[i])
            style_patches.append(style_p)
            norm_style_patches.append(norm_style_p)

            target_p, norm_target_p = self.extract_patches(
                target_features[i],
                patch_shape=(patch_shapes[i][0], patch_shapes[i][1],
                             K.int_shape(target_features[i])[-1]),
                strides=strides[i])
            target_patches.append(target_p)
            norm_target_patches.append(norm_target_p)

        # Defining all losses
        self.mrf_loss = self.get_mrf_loss(style_patches, norm_style_patches,
                                          target_patches, norm_target_patches,
                                          mrf_style_weights)
        self.total_variation_loss = (
            total_variation_weight *
            self.get_total_variation_loss(target_image_tensor))
        self.total_loss = self.mrf_loss + self.total_variation_loss

        evaluator = Evaluator(self)

        loss_grads = K.gradients(self.total_loss, target_image_tensor)
        # Evaluator class for computing efficiency
        outputs = [self.total_loss]

        if isinstance(loss_grads, (list, tuple)):
            outputs += loss_grads
        else:
            outputs.append(loss_grads)
        self.f_outputs = K.function([target_image_tensor], outputs)
        self.get_loss = K.function([target_image_tensor],
                                   [self.mrf_loss, self.total_variation_loss])

        # Initialize output image as random noise
        xt = np.random.uniform(0, 255, (1, self.image_height,
                                        self.image_width, self.image_depth))

        # Generate images by iterative optimization
        self.total_loss_history = []
        xt = vgg19.preprocess_input(xt)
        for i in range(N_max):
            print('Start of iteration', i)
            start_time = time.time()
            xt, min_val, info = fmin_l_bfgs_b(evaluator.loss, xt.flatten(),
                                              fprime=evaluator.grads,
                                              maxfun=30)
            mrf_loss_value, total_variation_loss_value = self.get_loss(
                [xt.reshape((1, self.image_height,
                             self.image_width, self.image_depth))])
            self.total_loss_history.append(min_val)
            msg = ('Total loss : {} || MRF loss : {} ||' +
                   ' Total variation loss : {}')
            print(msg.format(min_val, mrf_loss_value,
                             total_variation_loss_value))
            end_time = time.time()
            print('Iteration {} completed in {:2f}s\n'.format(
                i, end_time-start_time))

            if ((i >= 1 and
                 self.total_loss_history[-1] == self.total_loss_history[-2])):
                if i < 3:
                    warnings.warn("The method probably did not converge")
                break

        return self.deprocess_image(xt.copy())
